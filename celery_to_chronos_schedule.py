#!/usr/bin/env python
"""
Convert celery schedules to chronos schedules

(You may want to set the local_tz variable in the cron_to_iso8601() function to your local time-zone)
"""

from calendar import Calendar
from datetime import datetime
from itertools import product
from pprint import pprint

from celery.schedules import crontab
from isodate import D_DEFAULT, duration_isoformat
import pytz

# NOTE: Define or load these from somewhere ^_^
CELERYBEAT_SCHEDULE = {
    'Test-job': {
        'task': 'banana',
        'schedule': crontab(minute='*/5'),
        'kwargs': {
            'throw_this_many': 5,
        }
    },
}

def cron_runtimes(cron, year=datetime.now().year):
    """
    Return run-times defined by the cron, for the given year.
    """

    # Determine the days of each month, for the given year
    month_days = {}
    for month in cron.month_of_year:
        month_days[month] = [dom for dom in Calendar().itermonthdays(year, month) if dom > 0]
    
    run_days = []
    for month, day in product(cron.month_of_year, cron.day_of_month):
        # NOTE: celery.schedules.crontab expands cron.day_of_month to include days that don't
        # always exist. This takes care of that.
        if not day in month_days[month]:
            continue
        potential_day = datetime(year, month, day)
        cron_weekday = potential_day.isoweekday() % 7
        if cron_weekday in cron.day_of_week:
            run_days.append(potential_day)

    run_times = []
    for day, hour, minute in product(run_days, cron.hour, cron.minute):
        potential_time = day.replace(hour=hour, minute=minute)
        run_times.append(potential_time)

    del run_days[:]
    month_days.clear()
    return run_times

def closest_to(reference_time=datetime.now(), dates=None):
    """
    Using the given reference time and list of dates, return the index
    of the date that is closest to the reference time.
    """
    if dates is None:
        raise ValueError("closest_time() requires a list of dates!")

    closest = None
    closest_index = None
    index = 0
    for date in dates:
        distance = abs(reference_time - date)
        if closest is None:
            closest = distance
            closest_index = index
        else:
            if distance < closest:
                closest = distance
                closest_index = index
        index += 1

    return closest_index

def expand_cron(cron, distance=10, reference_time=datetime.now(), filter_future=True):
    """
    Given a cron object, expand it into potential run-times within <distance> runs of
    the reference time, minus any run-times that would extend into the future (by default).
    """

    current_time = datetime.now() # We won't return crons that are for the future

    # Expanded list of cron's run-times within the current year
    run_times = cron_runtimes(cron, reference_time.year)

    # Determine where on the list of run-times the reference time exists
    closest_runtime_index = closest_to(reference_time=reference_time, dates=run_times)

    # Pad the beginning of the run_times, until we have at least <distance> of them
    # on each side of the reference time.
    years_delta = 1
    while (closest_runtime_index + 1) < distance:
        run_times = cron_runtimes(cron, reference_time.year - years_delta) + run_times
        closest_runtime_index = closest_to(reference_time=reference_time, dates=run_times)
        years_delta += 1

    # Pad the end of the run_times, until we have at least <distance> of them
    # on each side of the reference time.
    years_delta = 1
    while (len(run_times) - 1) - closest_runtime_index < distance:
        run_times += cron_runtimes(cron, reference_time.year + years_delta)
        closest_runtime_index = closest_to(reference_time=reference_time, dates=run_times)
        years_delta += 1

    run_times_range = run_times[closest_runtime_index - distance:closest_runtime_index + distance]

    if filter_future:
        # Filter out run_times that exist in the future
        run_times_range = [dt for dt in run_times_range if dt < current_time]

    del run_times[:]

    return run_times_range

def cron_to_iso8601(cron):
    """
    Convert the given celery-cron to iso8601 repeating interval format
    """

    # NOTE: Set this to whatever your local time-zone is
    local_tz = 'Etc/GMT+8'
    zone = pytz.timezone(local_tz)

    unbounded_recurring_time_interval_format = "R/{start_time}/{duration}"

    # Get the next occurrances of the cron, to determine the start of the repeating interval
    current_dt = datetime.now()
    cron_run_schedule = expand_cron(cron, reference_time=current_dt, filter_future=False)
    # Filter out runs from the past
    future_cron_runs = [dt for dt in cron_run_schedule if dt > current_dt]
    next_run = future_cron_runs[0] # Will be used for reference time in iso_8601_string

    # Format for iso8601 times in chronos uses UTC
    next_run_lt = zone.localize(next_run)
    next_run_utc = next_run_lt.astimezone(pytz.UTC)
    next_utc_iso8601 = next_run_utc.isoformat()

    # The reference (start) time for the time string
    next_utc_iso8601 = next_utc_iso8601.replace("+00:00", "Z", 1)

    # Duration portion of time string
    period_between_runs = future_cron_runs[1] - next_run
    iso_8601_duration = duration_isoformat(period_between_runs, D_DEFAULT)

    repeat_iso_8601_string = unbounded_recurring_time_interval_format.format(**{"start_time": next_utc_iso8601,
                                                                                "duration": iso_8601_duration})

    return repeat_iso_8601_string
    
if __name__ == "__main__":
    celery_schedules = CELERYBEAT_SCHEDULE
    for name, config in celery_schedules.items():
        print("Schedule", name)
        pprint(config)
        iso_8601_interval = cron_to_iso8601(config["schedule"])
        print("chronos schedule", iso_8601_interval)
