You can use this script to help convert [celery-scheduler definitions](http://docs.celeryproject.org/en/latest/userguide/periodic-tasks.html#crontab-schedules) to [chronos](https://github.com/mesos/chronos) schedules, by converting the celery-schedule cron objects to [iso8601](https://en.wikipedia.org/wiki/ISO_8601#Repeating_intervals) repeating time-interval strings.

But if you're considering using chronos, please [read Kyle's blog post about it](https://aphyr.com/posts/326-jepsen-chronos) first.
